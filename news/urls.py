from django.urls import path
from . import views

urlpatterns = [
    path('', views.news, name="news"),
    path('<page_slug>/', views.slug, name='news_slug'),
    path('id/<int:news_id>/', views.id, name='news_id'),
    path('like_news/id/<int:news_id>/', views.like, name='like_news'),
    path('sort-by/categories/', views.sort, name="sort"),
    path('search/news/', views.search, name="search"),
]
