from django.urls import path
from . import views
from .views import testimoni, list_user

urlpatterns = [
    path('', views.about, name="aboutus"),
    path('testimoni/', testimoni, name="testimoni"),
    path('list_user/', list_user, name="list_user"),
]
