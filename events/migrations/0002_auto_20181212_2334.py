# Generated by Django 2.1.1 on 2018-12-12 16:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='events',
            name='participant',
            field=models.ManyToManyField(to='dashboard.User'),
        ),
    ]
