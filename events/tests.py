from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import ArticlesView, id
from .models import Events
# Create your tests here.


class eventTest(TestCase):
    def test_Events_url_is_exist(self):
        response = Client().get('/events/')
        self.assertEqual(response.status_code, 200)

    def test_random_url(self):
        response = Client().get('sssss')
        self.assertNotEqual(response.status_code, 200)

    def test_events_particular_url_is_exist(self):
        events = Events.objects.create(judul_events="hhe", desc_events="hehe")
        response = Client().get('/events/1')
        self.assertEqual(response.status_code, 301)

    def test_about_contains_judul(self):
        Event = Events.objects.create(judul_events="hhe", desc_events="hehe")
        response = Client().get('/events/')
        response_html = response.content.decode('utf8')
        self.assertContains(response, Event.judul_events)

    def test_model_can_create_Events(self):
        Event = Events.objects.create(judul_events="hhe", desc_events="hehe")
        count_all_variable = Events.objects.all().count()
        self.assertEqual(count_all_variable, 1)

    def test_Events_contains_events(self):
        response = Client().get('/events/')
        response_html = response.content.decode('utf8')
        self.assertContains(response, 'Events')
